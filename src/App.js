import React from "react";
import "./App.css";
import FadeIn from "react-fade-in";
import { FaHeart } from "react-icons/fa";  // Font Awesome

class App extends React.Component {
  state = {
    items: [
      { id: 1, text: "Promerner Josy" },
      { id: 2, text: "Faire les courses" }
    ],
    userInput: ""
  };

  handleDelete = id => {
    const items = [...this.state.items];
    const index = items.findIndex(item => item.id === id);
    items.splice(index, 1);
    this.setState({
      items
    });
  };

  handleChange = event => {
    event.preventDefault();
    const value = event.currentTarget.value;
    this.setState({
      userInput: value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const items = [...this.state.items];
    const id = new Date().getTime;
    const text = this.state.userInput;
    const item = { id, text };
    items.push(item);
    this.setState({
      items,
      userInput: ""
    });
  };

  render() {
    return (
    
      <div className="App container">
        <FadeIn delay={200}>
        <div className="heading">
          <img
            className="heading__img"
            src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/756881/laptop.svg"
            alt="boy"
          />
          <h1 className="heading__title">To-Do List</h1>
        </div>
        </FadeIn>
        <FadeIn delay={500}>
        <ul>
            {this.state.items.map(item => (
              <li key={item.id}>
                {item.text}
               
                <button className="close" onClick={this.handleDelete}>
                <FaHeart>
                  
                  </FaHeart>
                </button>
              </li>
            ))}
        </ul>
        </FadeIn>
        <FadeIn delay={300}>
        <form className="form" onSubmit={this.handleSubmit}>
          <label className="form__label" htmlfor="todo">
            Liste de Tâches 
          </label>
          <input
            required
            size="30"
            name="to-do"
            id="todo"
            type="text"
            className="form__input"
            value={this.state.userInput}
            onChange={this.handleChange}
          />
          <button className="button" type="submit">
            <span>Confirmer</span>
          </button>
        </form>
        </FadeIn>
        <div>
          <div>
            <ul className="toDoList"></ul>
          </div>
        </div>
      </div>

    );
  }
}

export default App;
